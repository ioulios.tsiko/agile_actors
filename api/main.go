package main

import (
	"agile/bankService"
	"agile/config"
	"agile/handler"
	"agile/store"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	_ "github.com/lib/pq"
)

func main() {
	e := echo.New()

	cnfg := config.LoadEnv()

	s := store.NewStore(cnfg.SQLString)

	bs := bankService.NewBankService(s)

	h := handler.NewHandler(s, bs)

	// set requests to timeout after 10 seconds
	e.Use(middleware.ContextTimeout(10 * time.Second))

	e.GET("/accounts", h.GetAccounts)
	e.GET("/transactions", h.GetTransactions)
	e.POST("/transfer", h.TransferHanlder)
	e.Logger.Fatal(e.Start(":" + cnfg.Port))
}
