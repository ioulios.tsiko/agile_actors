package handler

type TransferReq struct {
	Source int     `json:"source"`
	Target int     `json:"target"`
	Amount float64 `json:"amount"`
}

type ValidationError struct {
	Key   string `json:"key"`
	Error string `json:"error"`
}

func (tr *TransferReq) Validate() []ValidationError {
	ve := []ValidationError{}

	if tr.Target <= 0 {
		ve = append(ve, ValidationError{Key: "target", Error: "required field"})
	}

	if tr.Source <= 0 {
		ve = append(ve, ValidationError{Key: "source", Error: "required field"})
	}

	if tr.Source == tr.Target {
		ve = append(ve, ValidationError{Key: "target, source", Error: "transfer between same account is not allowed"})
	}

	if tr.Amount <= 0 {
		ve = append(ve, ValidationError{Key: "amount", Error: "amount must be positive"})
	}

	if len(ve) > 0 {
		return ve
	}

	return nil
}
