package handler

import (
	"agile/bankService"
	"agile/store"
	"net/http"
	"strings"

	"github.com/labstack/echo/v4"
	"github.com/lib/pq"
)

type Handler struct {
	store       store.Store
	bankService bankService.BankService
}

func NewHandler(s store.Store, bs bankService.BankService) Handler {
	return Handler{store: s, bankService: bs}
}

func (h *Handler) GetAccounts(c echo.Context) error {
	var httpStatusCode int
	var resp AccountsResponse

	a, err := h.store.GetAccounts(c.Request().Context())
	if err != nil {
		httpStatusCode = http.StatusInternalServerError
		resp.Success = false
		resp.ErrorMessage = "Interval server error"
	} else {
		resp.Success = true
		resp.Accounts = a
	}

	return c.JSON(httpStatusCode, resp)
}

func (h *Handler) GetTransactions(c echo.Context) error {
	var httpStatusCode int
	var resp TransactionsResponse

	t, err := h.store.GetTransactions(c.Request().Context())
	if err != nil {
		httpStatusCode = http.StatusInternalServerError
		resp.Success = false
		resp.ErrorMessage = "Interval server error"
	} else {
		resp.Success = true
		resp.Transactions = t
	}

	return c.JSON(httpStatusCode, t)
}

func (h *Handler) TransferHanlder(c echo.Context) error {
	var httpStatusCode int
	var resp Response

	var tr TransferReq

	if err := c.Bind(&tr); err != nil {
		resp.Success = false
		resp.ErrorMessage = "bad request"
		return c.JSON(http.StatusBadRequest, resp)
	}

	if verr := tr.Validate(); verr != nil {
		resp.Success = false
		resp.ErrorMessage = "bad request"
		resp.ValidationErrors = verr
		return c.JSON(http.StatusBadRequest, resp)
	}

	err := h.bankService.CompleteTransfer(c.Request().Context(), tr.Source, tr.Target, tr.Amount)
	if err != nil {
		// if transfer failed because of an sql error, return status 500 Interval server error
		if _, ok := err.(*pq.Error); ok || strings.HasPrefix(err.Error(), "sql:") {
			resp = Response{Success: false, ErrorMessage: "Interval server error"}
			httpStatusCode = http.StatusInternalServerError
		} else {
			resp = Response{Success: false, ErrorMessage: err.Error()}
			httpStatusCode = http.StatusNotAcceptable
		}
	} else {
		resp = Response{Success: true}
		httpStatusCode = http.StatusOK
	}

	return c.JSON(httpStatusCode, resp)
}
