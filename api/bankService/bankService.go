package bankService

import (
	"agile/store"
	"context"
	"errors"
)

type BankService interface {
	CompleteTransfer(ctx context.Context, sa_id int, ta_id int, amount float64) (err error)
}

func NewBankService(s store.Store) BankService {
	return &Bank{store: s}
}

type Bank struct {
	store store.Store
}

func (b *Bank) CompleteTransfer(ctx context.Context, sa_id int, ta_id int, amount float64) (err error) {
	if sa_id == ta_id {
		return errors.New("transfer between same account is not allowed")
	}

	err = b.store.CompleteTransfer(ctx, sa_id, ta_id, amount)

	return
}
