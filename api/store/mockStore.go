package store

import (
	"agile/model"
	"context"
)

type MockStore struct {
	CompleteTransferCalled bool
	ReturnError            error
}

func NewMockStore() *MockStore {
	return &MockStore{}
}

func (s *MockStore) GetAccounts(ctx context.Context) (accounts []model.Account, err error) {
	return
}

func (s *MockStore) GetTransactions(ctx context.Context) (transactions []model.Transaction, err error) {
	return
}

func (s *MockStore) CompleteTransfer(ctx context.Context, sa_id int, ta_id int, amount float64) (err error) {
	s.CompleteTransferCalled = true
	err = s.ReturnError
	return
}
