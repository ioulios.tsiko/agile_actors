package handler

import "agile/model"

type Response struct {
	Success          bool              `json:"success"`
	ErrorMessage     string            `json:"error_msg,omitempty"`
	ValidationErrors []ValidationError `json:"validation_errors,omitempty"`
}

type AccountsResponse struct {
	Response
	Accounts []model.Account `json:"accounts,omitempty"`
}

type TransactionsResponse struct {
	Response
	Transactions []model.Transaction `json:"transactions,omitempty"`
}
