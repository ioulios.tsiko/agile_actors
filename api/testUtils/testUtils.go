package testUtils

import (
	"net/http"
	"net/http/httptest"
	"reflect"
	"strings"
	"testing"

	"github.com/labstack/echo/v4"
)

func NewPostJSONRequest(body string) (c echo.Context, request *http.Request, response *httptest.ResponseRecorder) {
	e := echo.New()

	request, _ = http.NewRequest(http.MethodPost, "/", strings.NewReader(body))
	request.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)

	response = httptest.NewRecorder()
	c = e.NewContext(request, response)

	return
}

func AssertError(t testing.TB, err error, expected bool) {
	t.Helper()
	if expected && err == nil {
		t.Errorf("expected to get an error")
	} else if !expected && err != nil {
		t.Errorf("not expected to get an error but got '%s'", err.Error())
	}
}

func AssertHTTPStatus(t testing.TB, got, want int) {
	t.Helper()
	if got != want {
		t.Errorf("wanted http status %d but got http status %d", want, got)
	}
}

func AssertEqual(t testing.TB, got, want interface{}) {
	t.Helper()
	if !reflect.DeepEqual(got, want) {
		t.Errorf("wanted %q \nbut got %q", want, got)
	}
}

func AssertFunctionCalled(t testing.TB, called, want bool, funcName string) {
	t.Helper()
	if called != want {
		t.Errorf("wanted to call method '%s' but didn't, %t", funcName, called)
	}
}
