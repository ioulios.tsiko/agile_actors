--
-- PostgreSQL database dump
--

-- Dumped from database version 15.3
-- Dumped by pg_dump version 15.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: agile; Type: SCHEMA; Schema: -; Owner: agile
--

CREATE SCHEMA agile;


ALTER SCHEMA agile OWNER TO agile;

--
-- Name: enum_currency; Type: TYPE; Schema: agile; Owner: agile
--

CREATE TYPE agile.enum_currency AS ENUM (
    'EUR',
    'USD',
    'GBP',
    'JPY',
    'AUD'
);


ALTER TYPE agile.enum_currency OWNER TO agile;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: account; Type: TABLE; Schema: agile; Owner: agile
--

CREATE TABLE agile.account (
    account_id integer NOT NULL,
    balance double precision DEFAULT '0'::double precision NOT NULL,
    currency agile.enum_currency DEFAULT 'EUR'::agile.enum_currency NOT NULL,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    CONSTRAINT possitive_balance_constrain CHECK ((balance >= (0)::double precision))
);


ALTER TABLE agile.account OWNER TO agile;

--
-- Name: account_account_id_seq; Type: SEQUENCE; Schema: agile; Owner: agile
--

ALTER TABLE agile.account ALTER COLUMN account_id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME agile.account_account_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: trnsc; Type: TABLE; Schema: agile; Owner: agile
--

CREATE TABLE agile.trnsc (
    trnsc_id integer NOT NULL,
    source_account_id integer NOT NULL,
    target_account_id integer NOT NULL,
    amount double precision NOT NULL,
    currency agile.enum_currency NOT NULL,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    CONSTRAINT trsn_no_self_transfer_constrain CHECK ((source_account_id <> target_account_id))
);


ALTER TABLE agile.trnsc OWNER TO agile;

--
-- Name: transaction_transaction_id_seq; Type: SEQUENCE; Schema: agile; Owner: agile
--

ALTER TABLE agile.trnsc ALTER COLUMN trnsc_id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME agile.transaction_transaction_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Data for Name: account; Type: TABLE DATA; Schema: agile; Owner: agile
--

COPY agile.account (account_id, balance, currency, created_at) FROM stdin;
1	46	EUR	2023-05-22 16:06:48.483058
2	154	EUR	2023-05-22 16:06:48.483058
3	10	EUR	2023-05-22 16:06:48.483058
\.


--
-- Data for Name: trnsc; Type: TABLE DATA; Schema: agile; Owner: agile
--

COPY agile.trnsc (trnsc_id, source_account_id, target_account_id, amount, currency, created_at) FROM stdin;
6	1	2	2.75	EUR	2023-05-22 19:45:48.843371
7	1	2	2.75	EUR	2023-05-22 19:47:02.180421
8	1	2	2.75	EUR	2023-05-22 20:01:36.676607
9	1	2	2.75	EUR	2023-05-22 20:01:47.393779
10	1	2	2.75	EUR	2023-05-22 20:05:41.498345
11	1	2	2.75	EUR	2023-05-22 20:05:46.28163
12	1	2	2.75	EUR	2023-05-22 20:06:33.103309
13	1	2	2.75	EUR	2023-05-22 21:03:35.601115
14	1	2	3	EUR	2023-05-22 21:03:52.258397
15	1	2	4	EUR	2023-05-22 21:56:49.571319
16	1	2	5	EUR	2023-05-23 11:36:02.768619
17	1	2	5	EUR	2023-05-24 11:00:25.562099
18	1	2	5	EUR	2023-05-27 18:46:24.086144
19	1	2	5	EUR	2023-05-27 19:40:29.363859
20	1	2	5	EUR	2023-05-27 19:51:31.100808
\.


--
-- Name: account_account_id_seq; Type: SEQUENCE SET; Schema: agile; Owner: agile
--

SELECT pg_catalog.setval('agile.account_account_id_seq', 3, true);


--
-- Name: transaction_transaction_id_seq; Type: SEQUENCE SET; Schema: agile; Owner: agile
--

SELECT pg_catalog.setval('agile.transaction_transaction_id_seq', 20, true);


--
-- Name: account account_pkey; Type: CONSTRAINT; Schema: agile; Owner: agile
--

ALTER TABLE ONLY agile.account
    ADD CONSTRAINT account_pkey PRIMARY KEY (account_id);


--
-- Name: trnsc trnsc_pkey; Type: CONSTRAINT; Schema: agile; Owner: agile
--

ALTER TABLE ONLY agile.trnsc
    ADD CONSTRAINT trnsc_pkey PRIMARY KEY (trnsc_id);


--
-- Name: trnsc transaction_source_account_id_fkey; Type: FK CONSTRAINT; Schema: agile; Owner: agile
--

ALTER TABLE ONLY agile.trnsc
    ADD CONSTRAINT transaction_source_account_id_fkey FOREIGN KEY (source_account_id) REFERENCES agile.account(account_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: trnsc transaction_target_account_id_fkey; Type: FK CONSTRAINT; Schema: agile; Owner: agile
--

ALTER TABLE ONLY agile.trnsc
    ADD CONSTRAINT transaction_target_account_id_fkey FOREIGN KEY (target_account_id) REFERENCES agile.account(account_id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- PostgreSQL database dump complete
--

