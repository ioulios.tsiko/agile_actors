package model

type Account struct {
	ID        int
	Balance   float64
	Currency  string
	CreatedAt string
}

type Transaction struct {
	ID          int
	SourceAccID int
	TargetAccID int
	Amount      float64
	Currency    string
	CreatedAt   string
}
