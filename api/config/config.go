package config

import (
	"fmt"
	"os"

	"github.com/joho/godotenv"
)

const (
	Port      = "PORT"
	SQLString = "SQLSTRING"

	DefaultPort    = "8080"
	EnvFilePath    = ".env"
	EnvFileWarning = ".env file not found - working with values from system."
)

type Config struct {
	Port      string
	SQLString string
}

// LoadEnv loads the .env file and returns config object
func LoadEnv() (config Config) {
	if err := godotenv.Load(EnvFilePath); err != nil {
		fmt.Println(EnvFileWarning)
	}

	config.Port = os.Getenv(Port)
	if len(config.Port) == 0 {
		config.Port = DefaultPort
	}

	config.SQLString = os.Getenv(SQLString)

	return
}
