# AGILE ACTORS ASSIGNMENT

## API Service documentation
[POSTMAN Generated doc](https://documenter.getpostman.com/view/1990318/2s93m4ZPTD)


## Run Application with Docker Compose

### Environment variables
The programm loads a .env file on the running directory. If file does not exists, loads the system values dirextly  
Evn:  
* PORT: HTTP server container running port. **Required**
* DBPASS: Database password. **Required**

.env EX:
```
PORT=80
DBPASS=secretpass
```

### Run docker compose
```
cd ./docker
docker-compose up -d --build 
```

## **API Service**
API Servive to complete bank transactions.   
* For postrges database see section Bank DB

## Run
Service binary creates an http server on port 8080 by default. 

### Environment variables
The programm loads a .env file on the running directory. If file does not exists, loads the system values dirextly  
Evn:  
* PORT: HTTP server running port. 
  * Default:  8080
* SQLSTRING: SQL conection string. * If let empty, the programm runs with the **in memoty store**
  * Default: "" Empty String.
  * Format: `"postgres://username:password@host:port/database"`

.env EX:
```
PORT=8080
SQLSTRING=postgres://agile:secretpass@localhost/agileactors?sslmode=disable
```

### Run localy
```
cd ./api
go mod download
go build
./agile
```
### Run in docker container
```
cd ./api
docker build -t agile-api .
docker run -p 8080:8080 agile-api
```

## **Bank DB**
To use postgres database for the api load schema and init data from `./db/agile_init_db`


### Run in docker container
```
cd ./db
docker build -t agile-db .
docker run -p 8080:8080 agile-api
```


