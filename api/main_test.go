package main_test

import (
	"agile/bankService"
	"agile/config"
	"agile/handler"
	"agile/store"
	"agile/testUtils"
	"encoding/json"
	"net/http"
	"testing"
)

var (
	s  store.Store
	bs bankService.BankService
	h  handler.Handler
)

func setup() {
	cnfg := config.LoadEnv()

	s = store.NewStore(cnfg.SQLString)
	bs = bankService.NewBankService(s)
	h = handler.NewHandler(s, bs)
}

const (
	successTransferBody = `{
    "source":1,
    "target":2,
    "amount":10
	}`
	InsuficientFoundsErrorTransferBody = `{
    "source":1,
    "target":2,
    "amount":200
	}`
	SalfTransferErrorTransferBody = `{
    "source":1,
    "target":1,
    "amount":100
	}`
	SourceAccountNotFoundErrorTransferBody = `{
    "source":4,
    "target":1,
    "amount":100
	}`
	TargetAccountNotFoundErrorTransferBody = `{
    "source":1,
    "target":4,
    "amount":100
	}`
)

// Intgration test for POST /transfer
func TestTransferHandler(t *testing.T) {
	setup()

	t.Run("post new transfer successfully", func(t *testing.T) {

		c, _, res := testUtils.NewPostJSONRequest(successTransferBody)

		h.TransferHanlder(c)

		testUtils.AssertHTTPStatus(t, res.Code, http.StatusOK)

		want := handler.Response{Success: true}
		var got handler.Response
		json.NewDecoder(res.Body).Decode(&got)

		testUtils.AssertEqual(t, got, want)
	})

	t.Run("post new transfer with insuficient founds", func(t *testing.T) {

		c, _, res := testUtils.NewPostJSONRequest(InsuficientFoundsErrorTransferBody)

		h.TransferHanlder(c)

		testUtils.AssertHTTPStatus(t, res.Code, http.StatusNotAcceptable)

		want := handler.Response{Success: false, ErrorMessage: "Insuficient founds"}
		var got handler.Response
		json.NewDecoder(res.Body).Decode(&got)

		testUtils.AssertEqual(t, got, want)
	})

	t.Run("post new transfer in the same account", func(t *testing.T) {

		c, _, res := testUtils.NewPostJSONRequest(SalfTransferErrorTransferBody)

		h.TransferHanlder(c)

		testUtils.AssertHTTPStatus(t, res.Code, http.StatusBadRequest)

		verr := []handler.ValidationError{{Key: "target, source", Error: "transfer between same account is not allowed"}}
		want := handler.Response{Success: false, ErrorMessage: "bad request", ValidationErrors: verr}
		var got handler.Response
		json.NewDecoder(res.Body).Decode(&got)

		testUtils.AssertEqual(t, got, want)
	})

	t.Run("post new transfer with from non existing account", func(t *testing.T) {

		c, _, res := testUtils.NewPostJSONRequest(SourceAccountNotFoundErrorTransferBody)

		h.TransferHanlder(c)

		testUtils.AssertHTTPStatus(t, res.Code, http.StatusNotAcceptable)

		want := handler.Response{Success: false, ErrorMessage: "Source account not found"}
		var got handler.Response
		json.NewDecoder(res.Body).Decode(&got)

		testUtils.AssertEqual(t, got, want)
	})

	t.Run("post new transfer with to non existing account", func(t *testing.T) {

		c, _, res := testUtils.NewPostJSONRequest(TargetAccountNotFoundErrorTransferBody)

		h.TransferHanlder(c)

		testUtils.AssertHTTPStatus(t, res.Code, http.StatusNotAcceptable)

		want := handler.Response{Success: false, ErrorMessage: "Target account not found"}
		var got handler.Response
		json.NewDecoder(res.Body).Decode(&got)

		testUtils.AssertEqual(t, got, want)
	})

}
