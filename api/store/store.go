package store

import (
	db "agile/dummydb"
	"agile/model"
	"context"
	"database/sql"
	"fmt"
	"log"
)

const InMemmoryStoreWarning = "Running with in memmory store as sql string not provided"

type Store interface {
	GetAccounts(ctx context.Context) (accounts []model.Account, err error)
	GetTransactions(ctx context.Context) (transactions []model.Transaction, err error)
	CompleteTransfer(ctx context.Context, sa_id int, ta_id int, amount float64) (err error)
}

func NewStore(connStr string) (store Store) {
	if len(connStr) == 0 {
		fmt.Println(InMemmoryStoreWarning)
		store = &MemoryStore{db: db.DB1}
	} else {
		psqldb := connectDB(connStr)
		store = &PSQLStore{db: psqldb}
	}

	return
}

func connectDB(connStr string) *sql.DB {
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		log.Fatal(err)
	}

	// Check if connection is ok
	err = db.Ping()
	if err != nil {
		log.Fatalf("error pinging database: %v\n", err)
	}

	return db
}
