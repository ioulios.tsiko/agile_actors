package handler_test

import (
	"agile/bankService"
	"agile/handler"
	"agile/store"
	"agile/testUtils"
	"encoding/json"
	"errors"
	"net/http"
	"testing"

	"github.com/lib/pq"
)

var (
	s  store.Store
	bs *bankService.MockBank
	h  handler.Handler
)

func setup() {
	s = store.NewMockStore()
	bs = bankService.NewMockBankService()
	h = handler.NewHandler(s, bs)
}

const (
	successTransferBody = `{
    "source":1,
    "target":2,
    "amount":5
	}`
	InvalidTypesTransferBody = `{
    "source":"a",
    "target":2,
    "amount":5
	}`
	InvalidValuesTransferBody = `{
    "source":-1,
    "target":-1,
    "amount":-1
	}`
)

func TestTransferHandler(t *testing.T) {

	t.Run("post new transfer successfully", func(t *testing.T) {
		setup()

		c, _, res := testUtils.NewPostJSONRequest(successTransferBody)

		h.TransferHanlder(c)

		testUtils.AssertHTTPStatus(t, res.Code, http.StatusOK)

		want := handler.Response{Success: true}
		var got handler.Response
		json.NewDecoder(res.Body).Decode(&got)

		testUtils.AssertEqual(t, got, want)

		testUtils.AssertFunctionCalled(t, bs.CompleteTransferCalled, true, "CompleteTransfer")
	})

	t.Run("post new transfer with invalid body types", func(t *testing.T) {
		setup()

		c, _, res := testUtils.NewPostJSONRequest(InvalidTypesTransferBody)

		h.TransferHanlder(c)

		testUtils.AssertHTTPStatus(t, res.Code, http.StatusBadRequest)

		want := handler.Response{Success: false, ErrorMessage: "bad request"}
		var got handler.Response
		json.NewDecoder(res.Body).Decode(&got)

		testUtils.AssertEqual(t, got, want)

		testUtils.AssertFunctionCalled(t, bs.CompleteTransferCalled, false, "CompleteTransfer")
	})

	t.Run("post new transfer with invalid body values", func(t *testing.T) {
		setup()

		c, _, res := testUtils.NewPostJSONRequest(InvalidValuesTransferBody)

		h.TransferHanlder(c)

		testUtils.AssertHTTPStatus(t, res.Code, http.StatusBadRequest)
		verr := []handler.ValidationError{
			{Key: "target", Error: "required field"},
			{Key: "source", Error: "required field"},
			{Key: "target, source", Error: "transfer between same account is not allowed"},
			{Key: "amount", Error: "amount must be positive"},
		}
		want := handler.Response{Success: false, ErrorMessage: "bad request", ValidationErrors: verr}
		var got handler.Response
		json.NewDecoder(res.Body).Decode(&got)

		testUtils.AssertEqual(t, got, want)

		testUtils.AssertFunctionCalled(t, bs.CompleteTransferCalled, false, "CompleteTransfer")
	})

	t.Run("post new transfer with transaction error", func(t *testing.T) {
		setup()

		c, _, res := testUtils.NewPostJSONRequest(successTransferBody)

		var err = errors.New("Some error")
		bs.ReturnError = err

		h.TransferHanlder(c)

		testUtils.AssertHTTPStatus(t, res.Code, http.StatusNotAcceptable)

		want := handler.Response{Success: false, ErrorMessage: err.Error()}
		var got handler.Response
		json.NewDecoder(res.Body).Decode(&got)

		testUtils.AssertEqual(t, got, want)

		testUtils.AssertFunctionCalled(t, bs.CompleteTransferCalled, true, "CompleteTransfer")
	})

	t.Run("post new transfer with pq.Error", func(t *testing.T) {
		setup()

		c, _, res := testUtils.NewPostJSONRequest(successTransferBody)

		var err error
		err = &pq.Error{}
		bs.ReturnError = err

		h.TransferHanlder(c)

		testUtils.AssertHTTPStatus(t, res.Code, http.StatusInternalServerError)

		want := handler.Response{Success: false, ErrorMessage: "Interval server error"}
		var got handler.Response
		json.NewDecoder(res.Body).Decode(&got)

		testUtils.AssertEqual(t, got, want)

		testUtils.AssertFunctionCalled(t, bs.CompleteTransferCalled, true, "CompleteTransfer")
	})

	t.Run("post new transfer with sql error", func(t *testing.T) {
		setup()

		c, _, res := testUtils.NewPostJSONRequest(successTransferBody)

		var err = errors.New("sql: some error")
		bs.ReturnError = err

		h.TransferHanlder(c)

		testUtils.AssertHTTPStatus(t, res.Code, http.StatusInternalServerError)

		want := handler.Response{Success: false, ErrorMessage: "Interval server error"}
		var got handler.Response
		json.NewDecoder(res.Body).Decode(&got)

		testUtils.AssertEqual(t, got, want)

		testUtils.AssertFunctionCalled(t, bs.CompleteTransferCalled, true, "CompleteTransfer")
	})
}
