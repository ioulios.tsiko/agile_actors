package bankService

import (
	"context"
)

func NewMockBankService() *MockBank {
	return &MockBank{}
}

type MockBank struct {
	CompleteTransferCalled bool
	ReturnError            error
}

func (b *MockBank) CompleteTransfer(ctx context.Context, sa_id int, ta_id int, amount float64) (err error) {
	b.CompleteTransferCalled = true
	err = b.ReturnError

	return
}
