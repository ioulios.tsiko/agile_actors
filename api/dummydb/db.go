package db

import "agile/model"

type DB struct {
	Accounts     []model.Account
	Transactions []model.Transaction
}

var DB1 = DB{
	Accounts: []model.Account{
		{
			ID:        1,
			Balance:   100.00,
			Currency:  "EUR",
			CreatedAt: "2023-10-05 19:00",
		},
		{
			ID:        2,
			Balance:   100.00,
			Currency:  "EUR",
			CreatedAt: "2023-10-05 19:00",
		},
		{
			ID:        3,
			Balance:   0,
			Currency:  "EUR",
			CreatedAt: "2023-10-05 19:00",
		},
	},
	Transactions: []model.Transaction{},
}
