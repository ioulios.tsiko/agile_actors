package store

import (
	db "agile/dummydb"
	"agile/model"
	"context"
	"errors"
	"time"
)

type MemoryStore struct {
	db db.DB
}

func (s *MemoryStore) GetAccounts(ctx context.Context) (accounts []model.Account, err error) {
	return s.db.Accounts, nil
}

func (s *MemoryStore) GetTransactions(ctx context.Context) (transactions []model.Transaction, err error) {
	return s.db.Transactions, nil
}

func (s *MemoryStore) CompleteTransfer(ctx context.Context, sa_id int, ta_id int, amount float64) (err error) {

	var sa *model.Account
	var ta *model.Account

	for idx, a := range s.db.Accounts {
		if a.ID == sa_id {
			sa = &s.db.Accounts[idx]
		} else if a.ID == ta_id {
			ta = &s.db.Accounts[idx]
		}
	}

	if sa == nil {
		return errors.New("Source account not found")
	}
	if ta == nil {
		return errors.New("Target account not found")
	}

	if sa.Balance < amount {
		return errors.New("Insuficient founds")
	}

	sa.Balance -= amount
	ta.Balance += amount

	nextID := len(s.db.Transactions) + 1

	currentTime := time.Now()
	formattedTime := currentTime.Format("2006-01-02 15:04:05")

	t := &model.Transaction{
		ID:          nextID,
		SourceAccID: sa.ID,
		TargetAccID: ta.ID,
		Amount:      amount,
		Currency:    sa.Currency,
		CreatedAt:   formattedTime,
	}
	s.db.Transactions = append(s.db.Transactions, *t)

	return
}
