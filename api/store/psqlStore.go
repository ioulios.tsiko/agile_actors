package store

import (
	"agile/model"
	"context"
	"database/sql"
	"errors"
	"fmt"
)

type PSQLStore struct {
	db *sql.DB
}

func (s *PSQLStore) GetAccounts(ctx context.Context) (accounts []model.Account, err error) {
	accounts = []model.Account{}

	query := `select a.account_id, a.balance, a.currency,  TO_CHAR(a.created_at, 'YYYY-MM-DD HH24:MI:SS') as created_at
	from account a`

	rows, err := s.db.Query(query)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer rows.Close()

	for rows.Next() {
		var acc model.Account
		err = rows.Scan(
			&acc.ID,
			&acc.Balance,
			&acc.Currency,
			&acc.CreatedAt,
		)

		if err != nil {
			fmt.Println(err)
			return
		}
		accounts = append(accounts, acc)
	}

	return
}

func (s *PSQLStore) GetTransactions(ctx context.Context) (transactions []model.Transaction, err error) {
	transactions = []model.Transaction{}

	query := `select t.trnsc_id, t.source_account_id, t.target_account_id, t.amount, t.currency, TO_CHAR(t.created_at, 'YYYY-MM-DD HH24:MI:SS') as created_at
	from trnsc t`

	rows, err := s.db.Query(query)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer rows.Close()

	for rows.Next() {
		var trns model.Transaction
		err = rows.Scan(
			&trns.ID,
			&trns.SourceAccID,
			&trns.TargetAccID,
			&trns.Amount,
			&trns.Currency,
			&trns.CreatedAt,
		)

		if err != nil {
			fmt.Println(err)
			return
		}
		transactions = append(transactions, trns)
	}

	return
}

func (s *PSQLStore) CompleteTransfer(ctx context.Context, sa_id int, ta_id int, amount float64) (err error) {

	tx, err := s.db.BeginTx(ctx, nil)
	if err != nil {
		fmt.Println(err)
		return
	}

	// Defer a rollback in case anything fails.
	defer tx.Rollback()

	sa, ta, err := selectAccounts(tx, ctx, sa_id, ta_id)
	if err != nil {
		fmt.Println(err)
		return err
	}
	if sa == nil {
		return errors.New("Source account not found")
	}
	if ta == nil {
		return errors.New("Target account not found")
	}

	if sa.Balance < amount {
		return errors.New("Insuficient founds")
	}

	sb := sa.Balance - amount
	tb := ta.Balance
	if sa.Currency == ta.Currency {
		tb = ta.Balance + amount
	} else {
		//TODO Conver currency before subtracting
		tb = ta.Balance + amount
	}

	if ok, err := updateAccountBalance(tx, ctx, sa_id, sb); err != nil {
		fmt.Println(err)
		tx.Rollback()
		return err
	} else if !ok {
		tx.Rollback()
		return errors.New("Transaction failed")
	}

	if ok, err := updateAccountBalance(tx, ctx, ta_id, tb); err != nil {
		fmt.Println(err)
		tx.Rollback()
		return err
	} else if !ok {
		tx.Rollback()
		return errors.New("Transaction failed")
	}

	if ok, err := insertTransaction(tx, ctx, sa_id, ta_id, amount, ta.Currency); err != nil {
		fmt.Println(err)
		tx.Rollback()
		return err
	} else if !ok {
		tx.Rollback()
		return errors.New("Transaction failed")
	}

	if err = tx.Commit(); err != nil {
		return err
	}

	return
}

func selectAccounts(tx *sql.Tx, ctx context.Context, sa_id int, ta_id int) (sa *model.Account, ta *model.Account, err error) {
	query := `select a.account_id, a.balance, a.currency
	from account a
	where account_id = $1 OR account_id = $2 
	FOR UPDATE;`

	args := []interface{}{sa_id, ta_id}
	rows, err := tx.QueryContext(ctx, query, args...)
	if err != nil {
		return
	}
	defer rows.Close()

	for rows.Next() {
		var acc model.Account
		err = rows.Scan(
			&acc.ID,
			&acc.Balance,
			&acc.Currency,
		)

		if err != nil {
			return
		}

		if acc.ID == sa_id {
			sa = &acc
		} else if acc.ID == ta_id {
			ta = &acc
		}
	}

	return
}

func updateAccountBalance(tx *sql.Tx, ctx context.Context, sa_id int, balance float64) (ok bool, err error) {
	query := `update account  set balance = $1  where account_id = $2;`

	args := []interface{}{balance, sa_id}

	res, err := tx.ExecContext(ctx, query, args...)
	if err != nil {
		return
	}

	ra, err := res.RowsAffected()
	ok = ra == 1
	return
}

func insertTransaction(tx *sql.Tx, ctx context.Context, sa_id int, ta_id int, amount float64, currency string) (ok bool, err error) {
	query := `insert into trnsc (source_account_id, target_account_id, amount, currency) 
	VALUES($1, $2, $3, $4);`

	args := []interface{}{sa_id, ta_id, amount, currency}

	res, err := tx.ExecContext(ctx, query, args...)
	if err != nil {
		return
	}

	ra, err := res.RowsAffected()
	ok = ra == 1
	return
}
