package bankService_test

import (
	"agile/bankService"
	"agile/store"
	"agile/testUtils"
	"context"
	"errors"
	"testing"
	"time"
)

var (
	s  *store.MockStore
	bs bankService.BankService
)

func setup() {
	s = store.NewMockStore()
	bs = bankService.NewBankService(s)
}

func TestCompleteTransfer(t *testing.T) {

	t.Run("create new transfer successfully", func(t *testing.T) {
		setup()

		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()

		err := bs.CompleteTransfer(ctx, 1, 2, 5)

		testUtils.AssertError(t, err, false)

		testUtils.AssertFunctionCalled(t, s.CompleteTransferCalled, true, "CompleteTransfer")
	})

	t.Run("create new transfer with self transfer error", func(t *testing.T) {
		setup()

		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()

		selfTransferErr := errors.New("transfer between same account is not allowed")

		err := bs.CompleteTransfer(ctx, 1, 1, 5)

		testUtils.AssertError(t, err, true)
		testUtils.AssertEqual(t, err, selfTransferErr)
		testUtils.AssertFunctionCalled(t, s.CompleteTransferCalled, false, "CompleteTransfer")
	})

	t.Run("create new transfer  with store error", func(t *testing.T) {
		setup()

		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()

		returnErr := errors.New("some error")
		s.ReturnError = returnErr

		err := bs.CompleteTransfer(ctx, 1, 2, 5)

		testUtils.AssertError(t, err, true)
		testUtils.AssertEqual(t, err, returnErr)
		testUtils.AssertFunctionCalled(t, s.CompleteTransferCalled, true, "CompleteTransfer")
	})

}
